<h1 id="speakers" class="title">speakers</h1>
<div class="container-fluid speakers">
    <div class="container d-flex justify-content-center align-items-center flex-wrap">

        <div class="speaker">
            <img src="{{ asset('img/speakers/juan-senor.jpg') }}" alt="">
            <div class="speaker-name violet-bg">Juan Señor</div>
        </div>
        <div class="speaker">
            <img src="{{ asset('img/speakers/jay-ward.jpg') }}" alt="">
            <div class="speaker-name cyan-bg">Jay Ward</div>
        </div>
        <div class="speaker">
            <img src="{{ asset('img/speakers/jon-youshaei.jpg') }}" alt="">
            <div class="speaker-name light-green-bg">Jon Youshaei</div>
        </div>
        <div class="speaker">
            <img src="{{ asset('img/speakers/livia-fioretti.jpg') }}" alt="">
            <div class="speaker-name cyan-bg">Maxwell Luthy & Livia Fioretti</div>
        </div>
        <div class="speaker">
            <img src="{{ asset('img/speakers/oscar-alonso.jpg') }}" alt="">
            <div class="speaker-name violet-bg">Oscar Alonso</div>
        </div>
        <div class="speaker">
            <img src="{{ asset('img/speakers/liz-jackson.jpg') }}" alt="">
            <div class="speaker-name violet-bg">Liz Jackson</div>
        </div>
        <div class="speaker">
            <img src="{{ asset('img/speakers/thomas-kolster.jpg') }}" alt="">
            <div class="speaker-name cyan-bg">Thomas Kolster</div>
        </div>
        <div class="speaker">
            <img src="{{ asset('img/speakers/steve-keller.jpg') }}" alt="">
            <div class="speaker-name light-green-bg">Steve Keller</div>
        </div>
        <div class="speaker">
            <img src="{{ asset('img/speakers/andrea-arnau.jpg') }}" alt="">
            <div class="speaker-name cyan-bg">Andrea Arnau</div>
        </div>

    </div>
</div>

@include('modal-speaker')
