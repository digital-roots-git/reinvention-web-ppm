<h1 id="event" class="title">evento</h1>
<div class="container-fluid event desktop">
    <div class="row">
        <div class="col-6 p-0 left-item-wrapper">
            <div class="event-item shadow mt-5">
                <div class="post-it violet-bg-left left">
                    <div class="keynote-time">09:30 - 10:15</div>
                    <hr class="mt-1">
                    <div class="keynote-title">Goodvertising</div>
                    <hr>
                    <div class="keynote-speaker">Thomas Kolster</div>
                </div>
                <div class="event-pattern-left-1"><img src="{{ asset('img/event-pattern-left-1.svg') }}"></div>
            </div>
            <div class="event-item shadow">
                <div class="post-it light-green-bg-left left">
                    <div class="keynote-time">12:00 - 12:45</div>
                    <hr class="mt-1">
                    <div class="keynote-title">Honoring the Friction of Disability</div>
                    <hr>
                    <div class="keynote-speaker">Liz Jackson</div>
                </div>
                <div class="event-pattern-left-2"><img src="{{ asset('img/event-pattern-left-2.svg') }}"></div>
            </div>
            <div class="event-item shadow">
                <div class="post-it cyan-bg-left left"></div>
                <div class="event-pattern-left-3"><img src="{{ asset('img/event-pattern-left-3.svg') }}"></div>
            </div>
            <div class="event-item shadow">
                <div class="post-it violet-bg-left left"></div>
                <div class="event-pattern-left-4"><img src="{{ asset('img/event-pattern-left-4.svg') }}"></div>
            </div>
        </div>
        <div class="col-6 p-0 black-bg right-item-wrapper">
            <div class="event-item shadow event-title">
                <h2>key<br>notes</h2>
                <h3>día 1</h3>
            </div>
            <div class="event-item shadow">
                <div class="post-it cyan-bg-right right"></div>
                <div class="event-pattern-right-1"><img src="{{ asset('img/event-pattern-right-1.svg') }}"></div>
            </div>
            <div class="event-item shadow">
                <div class="post-it violet-bg-right right"></div>
            </div>
            <div class="event-item shadow">
                <div class="post-it light-green-bg-right right"></div>
                <div class="event-pattern-right-2"><img src="{{ asset('img/event-pattern-right-2.svg') }}"></div>
            </div>
            <div class="event-item shadow">
                <div class="post-it cyan-bg-right right"></div>
            </div>
        </div>



    </div>

    <div class="row position-relative">
        <div class="col-6 p-0 black-bg left-item-wrapper mt--5">
            <div class="event-item shadow event-title">
                <h2 class="text-right">work<br>shops</h2>
                <h3 class="text-right">día 2</h3>
                <div class="event-pattern-left-1"><img src="{{ asset('img/event-pattern-left-1.svg') }}"></div>
            </div>
            <div class="event-item shadow mt-5">
                <div class="post-it violet-bg-left left">
                    <div class="key-note-time">09:30 - 10:15</div>
                    <hr class="mt-1">
                    <div class="key-note-title">Goodvertising</div>
                    <hr>
                    <div class="key-note-speaker">Thomas Kolster</div>
                </div>
            </div>
            <div class="event-item shadow">
                <div class="post-it light-green-bg-left left">
                    <div class="keynote-time">12:00 - 12:45</div>
                    <hr class="mt-1">
                    <div class="keynote-title">Honoring the Friction of Disability</div>
                    <hr>
                    <div class="keynote-speaker">Liz Jackson</div>
                </div>
                <div class="event-pattern-left-2"><img src="{{ asset('img/event-pattern-left-2.svg') }}"></div>
            </div>
            <div class="event-item shadow">
                <div class="post-it cyan-bg-left left"></div>
                <div class="event-pattern-left-3"><img src="{{ asset('img/event-pattern-left-3.svg') }}"></div>
            </div>
            <div class="event-item shadow">
                <div class="post-it violet-bg-left left"></div>
                <div class="event-pattern-left-4"><img src="{{ asset('img/event-pattern-left-4.svg') }}"></div>
            </div>
        </div>
        <div class="col-6 p-0 right-item-wrapper">
            <div class="event-item shadow mt-5">
                <div class="post-it cyan-bg-right right"></div>
            </div>
            <div class="event-item shadow">
                <div class="post-it violet-bg-right right"></div>
                <div class="event-pattern-right-1"><img src="{{ asset('img/event-pattern-right-1.svg') }}"></div>
            </div>
            <div class="event-item shadow">
                <div class="post-it light-green-bg-right right"></div>
            </div>
            <div class="event-item shadow">
                <div class="post-it cyan-bg-right right"></div>
            </div>
        </div>
    </div>

    {{-- mobile --}}
</div>
<div class="container-fluid mobile event">
    <div class="row m-0">
        <div class="col-6 p-0 left-item-wrapper">
            <div class="post-it violet-bg-left mt-5 text-right">
                <div class="keynote-time">09:30 - 10:15</div>
                <hr>
                <div class="keynote-title">Goodvertising</div>
                <hr>
                <div class="keynote-speaker">Thomas Kolster</div>
            </div>

            <div class="post-it light-green-bg-left text-right">
                <div class="keynote-time">12:00 - 12:45</div>
                <hr>
                <div class="keynote-title">Honoring the Friction of Disability</div>
                <hr>
                <div class="keynote-speaker">Liz Jackson</div>
            </div>

            <div class="post-it cyan-bg-left text-right"></div>

            <div class="post-it violet-bg-left text-right"></div>
        </div>
        <div class="col-6 p-0 pb-4 black-bg right-item-wrapper">
            <div class="event-title">
                <h2>key<br>notes</h2>
                <h3>día 1</h3>
            </div>

            <div class="post-it cyan-bg-right"></div>
            <div class="post-it violet-bg-right"></div>
            <div class="post-it light-green-bg-right"></div>
            <div class="post-it cyan-bg-right mb-4"></div>
        </div>
    </div>

    <div class="row m-0">
        <div class="col-6 p-0 black-bg left-item-wrapper mt--5">
            <div class="event-title">
                <h2 class="text-right">work<br>shops</h2>
                <h3 class="text-right">día 2</h3>
            </div>

            <div class="post-it cyan-bg-right text-right"></div>
            <div class="post-it violet-bg-right text-right"></div>
            <div class="post-it light-green-bg-right text-right"></div>
            <div class="post-it cyan-bg-right mb-4 text-right"></div>
        </div>
        <div class="col-6 p-0 right-item-wrapper">
            <div class="post-it violet-bg-left mt-5">
                <div class="keynote-time">09:30 - 10:15</div>
                <hr>
                <div class="keynote-title">Goodvertising</div>
                <hr>
                <div class="keynote-speaker">Thomas Kolster</div>
            </div>

            <div class="post-it light-green-bg-left">
                <div class="keynote-time">12:00 - 12:45</div>
                <hr>
                <div class="keynote-title">Honoring the Friction of Disability</div>
                <hr>
                <div class="keynote-speaker">Liz Jackson</div>
            </div>

            <div class="post-it cyan-bg-left"></div>
            <div class="post-it violet-bg-left"></div>
        </div>
    </div>
</div>
