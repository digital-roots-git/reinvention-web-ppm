<div class="container-fluid home">
    <div class="d-flex justify-content-center align-items-center h-100 flex-column">
        <img src="{{ asset('img/logo.svg') }}" class="logo">
        <div class="slogan">Innovation in Business, Marketing and Entertainment</div>
    </div>
    <div class="home-pattern-up-left"><img src="{{ asset('img/home-pattern-up-left.svg') }}" class="img-fluid"></div>
    <div class="home-pattern-up-left-2"><img src="{{ asset('img/home-pattern-up-left-2.svg') }}" class="img-fluid"></div>
    <div class="home-pattern-bottom-left"><img src="{{ asset('img/home-pattern-bottom-left.svg') }}" class="img-fluid"></div>
    <div class="home-pattern-bottom-left-2"><img src="{{ asset('img/home-pattern-bottom-left-2.svg') }}" class="img-fluid"></div>

    <div class="home-pattern-up-right"><img src="{{ asset('img/home-pattern-up-right.svg') }}" class="img-fluid"></div>
    <div class="home-pattern-up-right-2"><img src="{{ asset('img/home-pattern-up-right-2.svg') }}" class="img-fluid"></div>
    <div class="home-pattern-bottom-right"><img src="{{ asset('img/home-pattern-bottom-right.svg') }}" class="img-fluid"></div>
    <div class="home-pattern-bottom-right-2"><img src="{{ asset('img/home-pattern-bottom-right-2.svg') }}" class="img-fluid"></div>
</div>
