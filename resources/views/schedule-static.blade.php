<h1 id="schedule" class="title">ESCOGE LA MODALIDAD QUE PREFIERAS</h1>
<div class="container-fluid schedule-static p-0">
    <div class="row mt-5">
        <div class="col-12 d-flex flex-column align-items-center">
            {{-- <h1 class="title-day">escoge la MODALIDAD QUE PREFIERAS:</h1> --}}
            <img src="{{ asset('img/schedule-static.png') }}" alt="schedule">
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 col-sm-12 violet-bg schedule-static-text">
            <h4>quito</h4>
            25 & 26 NOVIEMBRE
            <hr>
            QUORUM <br>
            PASEO SAN FRANCISCO
        </div>
        <div class="col-md-6 col-sm-12 cyan-bg schedule-static-text">
            <h4>GUAYAQUIL</h4>
            28 & 29 NOVIEMBRE
            <hr>
            SALÓN DE LOS PRESIDENTES <br>
            CENTRO DE CONVENCIONES
        </div>
    </div>

</div>
