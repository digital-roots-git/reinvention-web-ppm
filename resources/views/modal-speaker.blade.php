<div class="modal fade" id="modal-speaker" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-wrapper">
                <img src="" class="shadow">
                <div class="modal-info-wrapper">
                    <div class="modal-info">
                        <h2>conferencia</h2>
                        <div class="modal-subinfo">
                            <img src="" />
                            <div class="modal-postit violet-bg-left">
                                <div class="keynote-time">2:30 pm</div>
                                <hr class="mt-1">
                                <div class="keynote-title">skills in our techie world</div>
                                <hr>
                                <div class="keynote-speaker">Scott Hartley</div>
                            </div>
                        </div>
                        <p>
                            Scott Hartley ha tenido una gran trayectoria, desde trabajar en Google y Facebook, hasta ser inversor de riesgo en Silicon Valley.
                            En su conferencia “Human skills in our techie world”, Scott nos cuenta el secreto a la innovación que no es nada más que el
                            balance perfecto entre la tecnología y el arte.
                        </p>
                    </div>
                    <div class="modal-info">
                        <h2>workshop</h2>
                        <div class="modal-subinfo">
                            <div class="modal-postit cyan-bg-right">
                                <div class="keynote-time">2:30 pm</div>
                                <hr class="mt-1">
                                <div class="keynote-title">skills in our techie world</div>
                                <hr>
                                <div class="keynote-speaker">Scott Hartley</div>
                            </div>
                            <img src="" />
                        </div>
                        <div class="modal-social-media">
                            <div class="social-media-item">
                                <a href="#" target="_blank"><img src="{{ asset('img/in-fff.svg') }}" alt="instagram"/></a>
                                instagram.com/scottehartley
                            </div>
                            <div class="social-media-item">
                                <a href="#" target="_blank"><img src="{{ asset('img/fb-fff.svg') }}" alt="facebook"/></a>
                                facebook/scottehartley
                            </div>
                            <div class="social-media-item">
                                <a href="#" target="_blank"><img src="{{ asset('img/tw-fff.svg') }}" alt="twitter"/></a>
                                twitter/scottehartley
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
