<nav class="navbar sticky-top navbar-dark navbar-expand-lg">
    <a class="navbar-brand" href="#">Reinvention <span>|</span> 2019</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav">
            <li class="nav-item active">
                <a class="nav-link" href="#">About</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#speakers">Speakers</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#schedule">Agenda</a>
            </li>
        </ul>
    </div>
</nav>
<div class="empty-spacer"></div>
