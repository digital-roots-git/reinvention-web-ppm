<html>
    <body>
        <h1>Nuevo formulario de contacto.</h1>
        <h3>Datos:</h3>
        <ul>
            <li>Nombre completo: {{ $contact->fullname }}</li>
            <li>Ciudad: {{ $contact->city }}</li>
            <li>Email: {{ $contact->email }}</li>
            <li>Teléfono: {{ $contact->phone }}</li>
            <li>Mensaje: {{ $contact->message }}</li>
            <li>Fecha: {{ $contact->created_at }}</li>
        </ul>
    </body>
</html>
