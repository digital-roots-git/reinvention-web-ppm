<h1 id="contact" class="title">contacto</h1>
<div class="container-fluid contact">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-12">
                <div class="contact-title">¿necesitas ayuda?</div>
                <div class="contact-subtitle">No dudes en contactarnos</div>
                <div class="contact-mail"><a href="mailto:ventas@reinvention.la">ventas@reinvention.la</a></div>
                <div class="contact-social-media">
                    síguenos en:
                    <div class="social-media-icons d-flex align-items-center">
                        <a href="#" target="_blank"><img src="{{ asset('img/in.svg') }}" alt="instagram"/></a>
                        <a href="#" target="_blank"><img src="{{ asset('img/fb.svg') }}" alt="facebook"/></a>
                        <a href="#" target="_blank"><img src="{{ asset('img/tw.svg') }}" alt="twitter"/></a>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-12">
                @if (session('storeStatus'))
                    <div class="alert alert-success">
                        {{ session('storeStatus') }}
                    </div>
                @else
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form id="contact-form" action="{{ url('/contact') }}" method="POST" class="contact-form">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label>Nombre y apellido</label>
                            <input type="text" class="form-control" name="fullname">
                        </div>
                        <div class="form-group">
                            <label>Ciudad</label>
                            <input type="text" class="form-control" name="city">
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            <input type="email" class="form-control" name="email">
                        </div>
                        <div class="form-group">
                            <label>Teléfono</label>
                            <input type="text" class="form-control" name="phone">
                        </div>
                        <div class="form-group">
                            <label>Mensaje</label>
                            <textarea class="form-control" rows="5" name="message"></textarea>
                        </div>
                        <div class="d-flex align-items-center justify-content-center mt-4">
                            <button type="submit" class="btn btn-contact">Enviar</button>
                        </div>
                    </form>
                @endif
            </div>
        </div>
    </div>
</div>
