<h1 id="investment" class="title mt-5">Inversion</h1>
<div class="container-fluid investment mt-5">
    <div class="row mt-5 summary">
        <div class="row">
            <div>Conferencias</div>
            <div>Experiencia<br />Ultra</div>
            <div>Experiencia<br />Full</div>
        </div>
        <div class="row">
            <div>
                <div>Acceso a todas las conferencias del <strong>Día 1</strong></div>
            </div>
            <div>
                <div>
                    <div>Acceso a todas las conferencias del <strong>Día 1</strong></div>
                    <div class="plus-sign">+</div>
                    <div>1<br />Workshop</div>
                </div>
            </div>
            <div>
                <div>
                    <div>Acceso a todas las conferencias del <strong>Día 1</strong></div>
                    <div class="plus-sign">+</div>
                    <div>2<br />Workshops</div>
                </div>
            </div>
        </div>
    </div>
    <h2 class=subtitle>Una iniciativa disenada<br />para inspirarte</h2>
    <div class="row mt-5 only-mobile">
        <div class="title">Reinvention se adapta a tu día, a tu apretada agenda de trabajo, a tus tiempos libres de estudio porque ahora vas a poder customizar tu forma de inspirarte.</div>
        <div class="cnt">
            <div class="col left">
                DIA 1
                <hr />
                <span>REINVENTION FULL DAY</span>
                <hr />
                CONFERENCIAS
            </div>
            <div class="col right">
                Los asistentes recibirán contenido sobre nuevas tendencias y referencias globales sobre innovación, creatividad, tecnología y comunicación.
            </div>
        </div>
        <div class="cnt-2">
            <div class="col left">
                Se dictarán simultáneamente 3 workshops de distintas temáticas durante la mañana y 3 worskhops durante la tarde.
            </div>
            <div class="col right">
                DIA 2
                <hr />
                <span>REINVENTION +</span>
                <hr />
                WORKSHOPS & MASTERCLASSES
            </div>
        </div>
    </div>
    <div class="row mt-5 pricing">
        <div class="col-xl-4 col-left">
            <h1>VIP</h1>
            <p>Incluye: Registro preferencial, asientos en primeras filas, entrada a ceremonia y fiesta Lux Awards, almuerzo con conferencistas en el día 1, acceso a todas las conferencias del día 1.</p>
        </div>
        <div class="col-xl-8 col-right">
            <div class="w-100">
                <div class="left-header">Pagos hasta 24 de sept.</div>
                <div class="pseudo-table">
                    <div class="pt-row header">
                        <div># de personas</div>
                        <div>Conferencias</div>
                        <div>Ultra</div>
                        <div>Full</div>
                    </div>
                    <div class="pt-row data magenta">
                        <div>1 - 4</div>
                        <div>$275</div>
                        <div>$375</div>
                        <div>$475</div>
                    </div>
                    <div class="pt-row data magenta">
                        <div>5 o más</div>
                        <div>$250</div>
                        <div>$350</div>
                        <div>$450</div>
                    </div>
                    <div class="pt-row data magenta">
                        <div>Start-ups</div>
                        <div>N/A</div>
                        <div>N/A</div>
                        <div>N/A</div>
                    </div>
                </div>
            </div>
            <div class="w-100 mt-4">
                <div class="left-header">Pagos hasta 24 de oct.</div>
                <div class="pseudo-table">
                    <div class="pt-row data magenta">
                        <div>1 - 4</div>
                        <div>$300</div>
                        <div>$400</div>
                        <div>$500</div>
                    </div>
                    <div class="pt-row data magenta">
                        <div>5 o más</div>
                        <div>$275</div>
                        <div>$375</div>
                        <div>$475</div>
                    </div>
                    <div class="pt-row data magenta">
                        <div>Start-ups</div>
                        <div>N/A</div>
                        <div>N/A</div>
                        <div>N/A</div>
                    </div>
                </div>
            </div>
            <div class="w-100 mt-4">
                <div class="left-header">Pagos hasta 22 de nov.</div>
                <div class="pseudo-table">
                    <div class="pt-row data magenta">
                        <div>1 - 4</div>
                        <div>$325</div>
                        <div>$425</div>
                        <div>$525</div>
                    </div>
                    <div class="pt-row data magenta">
                        <div>5 o más</div>
                        <div>$300</div>
                        <div>$400</div>
                        <div>$500</div>
                    </div>
                    <div class="pt-row data magenta">
                        <div>Start-ups</div>
                        <div>N/A</div>
                        <div>N/A</div>
                        <div>N/A</div>
                    </div>
                </div>
            </div>
            <div class="w-100 mt-5 last">
                <div class="left-header">Pagos a partir del 25 de nov.</div>
                <div class="pseudo-table">
                    <div class="pt-row data magenta">
                        <div>$350</div>
                        <div>$450</div>
                        <div>$550</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="premium py-3 my-5">
        <div class="row mt-5 pricing">
            <div class="col-xl-4 col-left">
                <h1>Premium</h1>
                <p>Incluye: Acceso a todas las conferencias del día 1.</p>
            </div>
            <div class="col-xl-8 col-right">
                <div class="w-100">
                    <div class="left-header">Pagos hasta 24 de sept.</div>
                    <div class="pseudo-table">
                        <div class="pt-row header">
                            <div># de personas</div>
                            <div>Conferencias</div>
                            <div>Ultra</div>
                            <div>Full</div>
                        </div>
                        <div class="pt-row data green">
                            <div>1 - 4</div>
                            <div>$175</div>
                            <div>$275</div>
                            <div>$375</div>
                        </div>
                        <div class="pt-row data green">
                            <div>5 o más</div>
                            <div>$150</div>
                            <div>$250</div>
                            <div>$350</div>
                        </div>
                        <div class="pt-row data green">
                            <div>Start-ups</div>
                            <div>N/A</div>
                            <div>N/A</div>
                            <div>N/A</div>
                        </div>
                    </div>
                </div>
                <div class="w-100 mt-4">
                    <div class="left-header">Pagos hasta 24 de oct.</div>
                    <div class="pseudo-table">
                        <div class="pt-row data green">
                            <div>1 - 4</div>
                            <div>$200</div>
                            <div>$300</div>
                            <div>$400</div>
                        </div>
                        <div class="pt-row data green">
                            <div>5 o más</div>
                            <div>$175</div>
                            <div>$275</div>
                            <div>$375</div>
                        </div>
                        <div class="pt-row data green">
                            <div>Start-ups</div>
                            <div>N/A</div>
                            <div>N/A</div>
                            <div>N/A</div>
                        </div>
                    </div>
                </div>
                <div class="w-100 mt-4">
                    <div class="left-header">Pagos hasta 22 de nov.</div>
                    <div class="pseudo-table">
                        <div class="pt-row data green">
                            <div>1 - 4</div>
                            <div>$225</div>
                            <div>$325</div>
                            <div>$425</div>
                        </div>
                        <div class="pt-row data green">
                            <div>5 o más</div>
                            <div>$200</div>
                            <div>$300</div>
                            <div>$400</div>
                        </div>
                        <div class="pt-row data green">
                            <div>Start-ups</div>
                            <div>N/A</div>
                            <div>N/A</div>
                            <div>N/A</div>
                        </div>
                    </div>
                </div>
                <div class="w-100 mt-5 last">
                    <div class="left-header">Pagos a partir del 25 de nov.</div>
                    <div class="pseudo-table">
                        <div class="pt-row data green">
                            <div>$250</div>
                            <div>$350</div>
                            <div>$450</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="workshops py-3 my-5">
        <div class="row mt-5 pricing">
            <div class="col-xl-6 col-left">
                <h1>workshops</h1>
            </div>
            <div class="col-xl-6 col-right">
                <div class="w-100">
                    <div class="left-header">Pagos hasta 24 de sept.</div>
                    <div class="pseudo-table">
                        <div class="pt-row header">
                            <div># de personas</div>
                            <div>1 Workshop</div>
                            <div>2 Workshops</div>
                        </div>
                        <div class="pt-row data cyan">
                            <div>1 - 4</div>
                            <div>$225</div>
                            <div>$325</div>
                        </div>
                        <div class="pt-row data cyan">
                            <div>5 o más</div>
                            <div>$200</div>
                            <div>$200</div>
                        </div>
                        <div class="pt-row data cyan">
                            <div>Start-ups</div>
                            <div>N/A</div>
                            <div>N/A</div>
                        </div>
                    </div>
                </div>
                <div class="w-100 mt-4">
                    <div class="left-header">Pagos hasta 24 de oct.</div>
                    <div class="pseudo-table">
                        <div class="pt-row data cyan">
                            <div>1 - 4</div>
                            <div>$250</div>
                            <div>$350</div>
                        </div>
                        <div class="pt-row data cyan">
                            <div>5 o más</div>
                            <div>$225</div>
                            <div>$325</div>
                        </div>
                        <div class="pt-row data cyan">
                            <div>Start-ups</div>
                            <div>N/A</div>
                            <div>N/A</div>
                        </div>
                    </div>
                </div>
                <div class="w-100 mt-4">
                    <div class="left-header">Pagos hasta 22 de nov.</div>
                    <div class="pseudo-table">
                        <div class="pt-row data cyan">
                            <div>1 - 4</div>
                            <div>$275</div>
                            <div>$375</div>
                        </div>
                        <div class="pt-row data cyan">
                            <div>5 o más</div>
                            <div>$250</div>
                            <div>$350</div>
                        </div>
                        <div class="pt-row data cyan">
                            <div>Start-ups</div>
                            <div>N/A</div>
                            <div>N/A</div>
                        </div>
                    </div>
                </div>
                <div class="w-100 mt-5 last">
                    <div class="left-header">Pagos a partir del 25 de nov.</div>
                    <div class="pseudo-table">
                        <div class="pt-row data cyan">
                            <div>$300</div>
                            <div>$400</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
