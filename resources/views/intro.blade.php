<div class="container-fluid intro">
    <div class="container">
        <div class="row">
            <div class="col-md-12 d-flex justify-content-center align-items-center">
                <a href="https://sites.placetopay.ec/reinvention2019" target="_blank" class="btn rounded-0 btn-buy-tickets" id="REINV_BUY_UP">comprar entradas</a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="title">nos reinventamos</div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-12">
                <div class="intro-text">
                    <strong>RREINVENTION CAMBIA, PORQUE TÚ CAMBIAS</strong><br>
                    Por eso, ahora tú diseñas la experiencia a tu medida, agenda y presupuesto.
                    Todo en dos días de inmersión en contenido invaluable, reconocidos speakers y consultores globales, keynotes y workshops.
                </div>
            </div>
            <div class="col-lg-3 col-md-12 d-flex day-container">
                <div class="day-wrapper d-flex flex-column">
                    <div class="day violet">día</div>
                    <div class="day-number violet">1</div>
                </div>
                <div class="day-info-wrapper violet-bg">
                    <div class="day-info-title">Keynotes</div>
                    <hr>
                    <div class="day-info-subtitle">conferencias</div>
                </div>
            </div>
            <div class="col-lg-3 col-md-12 d-flex day-container">
                <div class="day-wrapper d-flex flex-column">
                    <div class="day cyan">día</div>
                    <div class="day-number cyan">2</div>
                </div>
                <div class="day-info-wrapper cyan-bg">
                    <div class="day-info-title">Workshops</div>
                    <hr>
                    <div class="day-info-subtitle">WORKSHOPS & MASTERCLASSES</div>
                </div>
            </div>
        </div>
    </div>
</div>
