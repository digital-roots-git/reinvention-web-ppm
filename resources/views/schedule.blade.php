<h1 id="schedule" class="title">Agenda</h1>
<div class="container-fluid schedule mt-5">
    <div class="row mt-5">
        <div class="col-lg-12 col-xl-6 col-left d-flex flex-column align-items-center">
            <h1 class="title-day">dia 1<br />keynotes only</h1>
            <table class="keynotes">
                <tbody>
                    <tr>
                        <td class="green-border" style="width: 8rem;">07:30 - 09:00</td>
                        <td class="spacer">&nbsp</td>
                        <td class="green-bg" colspan="2">Registro</td>
                    </tr>
                    <tr>
                        <td>09:00 - 09:30</td>
                        <td class="spacer">&nbsp</td>
                        <td class="text-right" colspan="2">Opening por Juan Señor</td>
                    </tr>
                    <tr>
                        <td class="green-border">09:30 - 10:15</td>
                        <td class="spacer">&nbsp</td>
                        <td class="green-bg white-right-border" style="width: 16rem;">Thomas Kolster</td>
                        <td class="green-bg text-left">Goodvertising</td>
                    </tr>
                    <tr class="spacer">
                        <td>&nbsp;</td>
                        <td class="spacer">&nbsp</td>
                        <td colspan="2">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="green-border">10:15 - 11:00</td>
                        <td class="spacer">&nbsp</td>
                        <td class="green-bg white-right-border">Maxwell Luthy</td>
                        <td class="green-bg text-left">The Future of Experiences | 5 Key Trends</td>
                    </tr>
                    <tr>
                        <td>11:00 - 12:00</td>
                        <td class="spacer">&nbsp</td>
                        <td class="text-right" colspan="2">Coffee Break</td>
                    </tr>
                    <tr>
                        <td class="green-border">12:00 - 12:45</td>
                        <td class="spacer">&nbsp</td>
                        <td class="green-bg white-right-border">Liz Jackson</td>
                        <td class="green-bg text-left">Honoring the Friction of Disability</td>
                    </tr>
                    <tr class="spacer">
                        <td>&nbsp;</td>
                        <td class="spacer">&nbsp</td>
                        <td colspan="2">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="green-border">12:45 - 13:30</td>
                        <td class="spacer">&nbsp</td>
                        <td class="green-bg white-right-border">Jon Youashei</td>
                        <td class="green-bg text-left">Cracking the code on going viral</td>
                    </tr>
                    <tr>
                        <td>13:30 - 15:00</td>
                        <td class="spacer">&nbsp</td>
                        <td class="text-right" colspan="2">Free Lunch</td>
                    </tr>
                    <tr>
                        <td class="green-border">15:00 - 15:45</td>
                        <td class="spacer">&nbsp</td>
                        <td class="green-bg white-right-border">TBC</td>
                        <td class="green-bg text-left">TBC</td>
                    </tr>
                    <tr class="spacer">
                        <td>&nbsp;</td>
                        <td class="spacer">&nbsp</td>
                        <td colspan="2">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="green-border">15:45 - 16:30</td>
                        <td class="spacer">&nbsp</td>
                        <td class="green-bg white-right-border">TBC</td>
                        <td class="green-bg text-left">TBC</td>
                    </tr>
                    <tr>
                        <td>16:30 - 17:15</td>
                        <td class="spacer">&nbsp</td>
                        <td class="text-right" colspan="2">Coffee Break</td>
                    </tr>
                    <tr>
                        <td class="green-border">17:15 - 18:00</td>
                        <td class="spacer">&nbsp</td>
                        <td class="green-bg white-right-border">TBC</td>
                        <td class="green-bg text-left">TBC</td>
                    </tr>
                    <tr class="spacer">
                        <td>&nbsp;</td>
                        <td class="spacer">&nbsp</td>
                        <td colspan="2">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="green-border">18:00 - 18:45</td>
                        <td class="spacer">&nbsp</td>
                        <td class="green-bg white-right-border">Hafiz Huda</td>
                        <td class="green-bg text-left">TBC</td>
                    </tr>
                    <tr>
                        <td>18:45 - 19:00</td>
                        <td class="spacer">&nbsp</td>
                        <td class="text-right" colspan="2">Cierre por Juan Señor</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="col-lg-12 col-xl-6 col-right d-flex flex-column align-items-center">
            <h1 class="title-day">dia 2<br />workshops only</h1>
            <table class="workshops">
                <thead>
                    <tr>
                        <th style="width: 9rem;">&nbsp;</th>
                        <th class="spacer" style="font-size: 1px; padding: 0; width: 1px;">&nbsp;</th>
                        <th>SALA 1</th>
                        <th>SALA 2</th>
                        <th>SALA 3</th>
                    </tr>
                    <tr>
                        <th>&nbsp;</th>
                        <th class="spacer">&nbsp;</th>
                        <th>“To be confirmed”<br />Trainer: To be confirmed</th>
                        <th>“Build the Experiences of the Future”<br />Trainers: Maxwell Luthy & Lisa Feierstein</th>
                        <th>“Cracking the code on going viral”<br />Trainer: Jon Youashei</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="cyan-border">08:30 - 09:00</td>
                        <td class="spacer">&nbsp;</td>
                        <td class="cyan-bg" colspan="3">Registro</td>
                    </tr>
                    <tr>
                        <td>09:00 - 10:30</td>
                        <td class="spacer">&nbsp;</td>
                        <td colspan="4">Workshop</td>
                    </tr>
                    <tr>
                        <td class="cyan-border">10:30 - 11:00</td>
                        <td class="spacer">&nbsp;</td>
                        <td class="cyan-bg" colspan="3">Coffee Break</td>
                    </tr>
                    <tr>
                        <td>11:00 - 13:00</td>
                        <td class="spacer">&nbsp;</td>
                        <td colspan="4">Workshop</td>
                    </tr>
                    <tr>
                        <td class="cyan-border">13:00 - 14:00</td>
                        <td class="spacer">&nbsp;</td>
                        <td class="cyan-bg" colspan="3">Free Lunch</td>
                    </tr>
                    <tr class="special">
                        <td>&nbsp;</td>
                        <td class="spacer">&nbsp;</td>
                        <td>“Human-Centric Digital Transformation”<br />Trainer: Kate O’Neill</td>
                        <td>“Stop selling shit! Let’s make work that matters”<br />Trainer: Thomas Kolster</td>
                        <td>“To be confirmed”<br />Trainer: Oscar Alonso</td>
                    </tr>
                    <tr>
                        <td class="magenta-border">14:00 - 14:00</td>
                        <td class="spacer">&nbsp;</td>
                        <td class="magenta-bg" colspan="3">Registro</td>
                    </tr>
                    <tr>
                        <td>14:30 - 16:00</td>
                        <td class="spacer">&nbsp;</td>
                        <td colspan="4">Workshop</td>
                    </tr>
                    <tr>
                        <td class="magenta-border">16:00 - 16:30</td>
                        <td class="spacer">&nbsp;</td>
                        <td class="magenta-bg" colspan="3">Coffee Break</td>
                    </tr>
                    <tr>
                        <td>16:30 - 18:30</td>
                        <td class="spacer">&nbsp;</td>
                        <td colspan="4">Workshop</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
