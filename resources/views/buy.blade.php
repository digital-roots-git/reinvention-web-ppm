<div class="container-fluid buy">
    <div class="row d-flex justify-content-center align-items-center">
        <div class="col-12 d-flex justify-content-center align-items-center">
            <div class="position-relative">
                <a href="https://sites.placetopay.ec/reinvention2019" target="_blank" class="btn btn-buy" id="REINV_BUY_DOWN">comprar</a>
                <div class="buy-patter-1"><img src="{{ asset('img/buy-pattern-1.svg') }}" /></div>
                <div class="buy-patter-2"><img src="{{ asset('img/buy-pattern-2.svg') }}" /></div>
            </div>
        </div>
    </div>
</div>
