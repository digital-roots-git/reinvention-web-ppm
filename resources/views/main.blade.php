<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <link rel="icon" href="../../../../favicon.ico">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="{{ mix('css/app.css') }}?v=1.0">

        <title>Reinvention 2019</title>
        <meta name="description" content="">
        <meta name="author" content="Digital Roots">
        <!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-5TT2NFC');</script>
        <!-- End Google Tag Manager -->
    </head>

    <body>
        <!-- Google Tag Manager (noscript) -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5TT2NFC"
            height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->
        <div class="container-fluid p-0">
            @include('menu')
            @include('home')
            @include('intro')
            @include('speakers')
            @include('schedule-static')
            @include('buy')
        </div>

        <!-- App JS -->
        <script type="text/javascript" src="{{ mix('js/app.js') }}"></script>
    </body>
</html>
