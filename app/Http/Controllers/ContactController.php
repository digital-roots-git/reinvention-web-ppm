<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

use App\Contact;
use App\Mail\ContactForm;

use Validator;

class ContactController extends Controller
{
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'fullname' => 'required|max:120',
            'city'     => 'required|max:60',
            'email'    => 'required|email|max:100',
            'phone'    => 'required|max:10|min:9',
            'message'  => 'required',
        ]);

        $validator->setAttributeNames([
            'fullname' => 'nombre completo',
            'city'     => 'ciudad',
            'email'    => 'email',
            'phone'    => 'teléfono',
            'message'  => 'mensaje',
        ]);

        if ($validator->fails()) {
            return redirect()->to(url()->previous() . '#contact')
                ->withErrors($validator)
                ->withInput();
        }

        $validatedData = $validator->validate();

        $contact = new Contact();
        $contact->fullname = $validatedData['fullname'];
        $contact->city = $validatedData['city'];
        $contact->email = $validatedData['email'];
        $contact->phone = $validatedData['phone'];
        $contact->message = $validatedData['message'];
        $contact->save();

        Mail::to(env('CONTACT_FORM_MAIL'))->send(new ContactForm($contact));

        return redirect()->to(url()->previous() . '#contact')
            ->with('storeStatus', 'Formulario enviado con éxito.');
    }
}
